'use strict';

const koa = require('koa');
const logger = require('koa-logger');
const router = require('koa-router')();
const cors = require('koa-cors');
const body = require('koa-better-body');

const HttpError = require('./models/HttpError');
const config = require('./config')(process.env.NODE_ENV);

const app = koa();

app.use(function* (next) {
  try {
    this.path = this.path.replace(/\/\//g, '/');
    let path = this.path.match(/^\/score\/(\w+)/i);
    if (path) {
      this.path = this.path.replace(/(^\/score\/)/i, '/');
    } else {
        throw new HttpError(404, 'Incorrect request');
    }
    this.type = 'application/x-www-form-urlencoded';
    yield next;
  } catch (e) {
    this.status = 500;
    this.body = e.message;
    this.type = 'text/plain; charset=utf-8';

    e = e || {};
    if (e.status || e.statusCode || e.code) this.status = e.status || e.statusCode || e.code;

    console.error(e);
  }
});

app.use(logger());
app.use(cors());
app.use(body({
  querystring: require('qs')
}));

app.use(config.configure);
app.use(require('./controllers').routes());

app.on('error', (err, ctx) => {
  console.error('server error', err, ctx);
});

app.server = app.listen(config.port, () => {
  console.log('Server started on 127.0.0.1:' + config.port);
});
