var HttpError = require('../models/HttpError');

var config = null;

module.exports = (env) => {
  if (config) { return config; }

  env = env || 'develop';
  config = require('./' + env);
  config.env = env;

  config.configure = function *(next) {
    this.config = config;

    yield next;
  };

  return config;
};
