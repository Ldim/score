'use strict';

const router = require('koa-router')();

const HttpError = require('../models/HttpError');
const Tournament = require('../models/Tournament');
const Player = require('../models/Player');

router.post('/fund', function* () {
  yield wrap(this, ctx => (player(ctx).save()));
});

router.post('/take', function* () {
  yield wrap(this, ctx => (player(ctx).take()));
});

router.get('/balance', function* () {
  yield wrap(this, ctx => (player(ctx).find()), ['playerId', 'balance']);
});

router.post('/announceTournament', function* () {
  yield wrap(this, ctx => (tournament(ctx).save()));
});

router.post('/joinTournament', function* () {
  yield wrap(this, join);
});

router.get('/resultTournament', function* () {
  yield wrap(this, win);
});

router.post('/reset', function* () {
  yield wrap(this, ctx => (new Tournament().reset()));
});

router.post('/gc', function* () {
  yield wrap(this, ctx => (new Player().gc()));
});

function* wrap(ctx, proc, exc = []) {
  try {
    let res = yield proc(ctx);
    let b = {};
    exc.forEach(e => {b[e] = res[e];})
    ctx.body = exc.length ? b : res; 
    console.log(ctx.body);
    ctx.type = 'application/json';
  } catch (e) {
    console.error(e);
    ctx.body = e;
  }
}

function* win(ctx) {
  let t = yield tournament(ctx).find();
  let tId = ctx.tournament.tournamentId;
  if (t == null) throw new HttpError(200, `Tournament ${tId} not found`);
  if (t.joined == undefined) throw new HttpError(200, `No players for tournament ${tId}`);
  let winner = t.joined[Math.floor(Math.random() * t.joined.length)];
  winner.amount += t.deposit;
  let losers = [];
  t.joined.forEach((e) => {
    if (e != winner) losers.push(e.id);
  }, this);
  console.log(losers);
  let p = new Player()
  yield p.win(tId, winner.amount / winner.backed, losers);
  yield p.los(tId, losers);
  return { playerId: winner.id, prize: winner.amount };
}

function* join(ctx) {
  let t = yield tournament(ctx).find();
  let tId = ctx.tournament.tournamentId;
  if (t == null) throw new HttpError(200, `Tournament ${tId} not found`);
  player(ctx);
  let backed = 1;
  if (ctx.query.backerId == undefined) {
    yield _take(ctx, tId, t.deposit, ctx.query.playerId);
  } else {
    let ids = ctx.query.backerId.split(',');
    backed = ids.length + 1;
    let bid = Number(t.deposit / backed);
    yield _take(ctx, tId, bid, ctx.query.playerId);
    for (let i = 0; i < ids.length; i++) {
      ctx.player.setId(ids[i]);
      yield _take(ctx, tId, bid, ctx.query.playerId);
    }
  }
  return yield ctx.tournament.join(ctx.query.playerId, t.deposit, backed);
}

function* _take(ctx, id, amount, backedId) {
  if ((yield ctx.player.takeById(id, -amount, backedId)).value == null) {
    throw new HttpError(200, `Player ${ctx.player.playerId} join error`);
  }
}

function tournament(ctx) {
  if (!ctx.querystring) throw new HttpError(200, 'No parameters');
  if (ctx.query.tournamentId == undefined) throw new HttpError(200, 'Tournament not specified');
  if (ctx.query.deposit != undefined) {
    var deposit = parseInt(ctx.query.deposit);
    if (isNaN(deposit)) throw new HttpError(200, 'Deposit must be a number');
  }
  return (ctx.tournament = new Tournament(ctx.query.tournamentId, deposit));
}

function player(ctx) {
  if (!ctx.querystring) throw new HttpError(200, 'No parameters');
  if (ctx.query.playerId == undefined) throw new HttpError(200, 'Player not specified');
  if (ctx.query.points != undefined) {
    var points = parseInt(ctx.query.points);
    if (isNaN(points)) throw new HttpError(200, 'Points must be a number');
  }
  return (ctx.player = new Player(ctx.query.playerId, points));
}

module.exports = router;