'use strict';

const co = require('co');
const mongo = require('mongoose');

const db = require('../services/connection');


class Entity {
    constructor(name) {
        this._name = name;
    }

    get _self() {
        let result = Object.assign({}, this);
        delete result._name;
        return result;
    }

    _id(id) {
        return mongo.Types.ObjectId(id);
    }

    _uno(op, arg, proc = (err, res) => (res)) {
        return co(function* () {
            return yield callback =>
                db.collection(this._name)[op](
                    arg,
                    (err, res) => callback(err, proc(err, res))
                );
        }.bind(this));
    }

    _deuce(op, arg, act, opt, proc = (err, res) => (res)) {
        return co(function* () {
            return yield callback =>
                db.collection(this._name)[op](
                    arg, act, opt,
                    (err, res) => callback(err, proc(err, res))
                );
        }.bind(this));
    }
}

module.exports = Entity;