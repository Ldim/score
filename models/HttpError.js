'use strict';

class HttpError extends Error {
  constructor(code, message, obj, err) {
    if (code instanceof Error) {
      err = code;
      code = err.code || 500;
      message = message || err.message;
    }

    if (obj instanceof Error) {
      err = obj;
      obj = undefined;
    }

    if (typeof code == 'string' && parseInt(code) !== code) {
      err = message instanceof Error ? message : err;
      message = code;
      code = err && err.code ? err.code : 500;
    }

    super(message);

    if (typeof obj == 'object') {
      Object.assign(this, obj);
    }

    this.code = parseInt(code) || 500;
    this.error = err;
    this.Success = 'False';
    this.ErrCode = message;
  }
}

module.exports = HttpError;
