'use strict';

const Entity = require('./Entity');

class Player extends Entity {
    constructor(playerId, balance) {
        super('players');
        this.playerId = playerId;
        this.balance = balance;
    }

    setId(playerId) {
        this.playerId = playerId;
    }

    find(playerId = this.playerId) {
        return this._uno('findOne', { playerId });
    }

    delete(playerId = this.playerId) {
        return this._uno('deleteOne', { playerId });
    }

    save(playerId = this.playerId) {
        return this._deuce('updateOne', { playerId }, this._self, { upsert: true });
    }

    take(amount = this.balance, playerId = this.playerId) {
        return this._deuce('findOneAndUpdate',
            { playerId, balance: { $gt: amount } },
            { $inc: { balance: amount } });
    }

    takeById(id, amount, backedId, playerId = this.playerId, ttl = 300000) {
        return this._deuce('findOneAndUpdate',
            { 'reserved_by.id': { $ne: id }, playerId, balance: { $gt: amount } },
            {
                $inc: { balance: amount },
                $push: { reserved_by: { id, amount, backedId, expire: Date.now() + ttl } }
            });
    }

    win(id, amount, losers) {
        return this._deuce('updateMany',
            { 'reserved_by.id': { $eq: id }, 'reserved_by.backedId': { $nin: losers } },
            {
                $inc: { balance: amount },
                $pull: { reserved_by: { id } }
            });
    }

    los(id, losers) {
        return this._deuce('updateMany',
            { 'reserved_by.id': { $eq: id }, 'reserved_by.backedId': { $in: losers } },
            {
                $pull: { reserved_by: { id } }
            });
    }

    gc() {
        return this._uno('aggregate', [
            { $match: { 'reserved_by.expire': { $lt: Date.now() } } },
            { $unwind: '$reserved_by' },
            { $addFields: { balance: { $add: ['$balance', '$reserved_by.amount'] } } },
            { $project: { 'reserved_by': 0 } },
            { $out: 'players' }]);
    }     
}

module.exports = Player;