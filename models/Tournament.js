'use strict';

const Entity = require('./Entity');

class Tournament extends Entity {
    constructor(tournamentId, deposit) {
        super('tournaments');
        this.tournamentId = tournamentId;
        this.deposit = deposit;
    }

    find(tournamentId = this.tournamentId) {
        return this._uno('findOne', {tournamentId});
    }

    delete(tournamentId = this.tournamentId) {
        return this._uno('deleteOne', {tournamentId});
    }

    save(tournamentId = this.tournamentId) {
        return this._deuce('updateOne', {tournamentId}, this._self, { upsert : true });
    }

    join(id, amount, backed, tournamentId = this.tournamentId, ttl = 300000) {
        return this._deuce('findOneAndUpdate',
            { 'joined.id': { $ne: id }, tournamentId },
            {
                $push: { joined: { id, amount, backed, expire: Date.now() + ttl } }
            });
    }

    reset() {
        return this._deuce('updateMany',
            { 'joined': { $ne: null } },
            {
                $pull: { joined: { } }
            });
    }
}

module.exports = Tournament;