'use strict';

const mongoose = require('mongoose');

const config = require('../config')(process.env.NODE_ENV);

mongoose.connect(config.db_url);

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error : '));
db.once('open', function(){
  console.log('Connected to ' + config.db_url);
});

module.exports = db;